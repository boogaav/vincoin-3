<?php
/**
 * Created by PhpStorm.
 * User: makovetski_artem
 * Date: 18.05.2018
 * Time: 15:54
 */

namespace App\Services\Localization;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\App;

class LocalizationHelper
{
    public static function getRouteLocale()
    {
        $locale = request()->segment(1);

        if (self::validateLocale($locale)) {
            return $locale;
        } else {
            return null;
        }
    }

    public static function getRoutePrefix()
    {
        if (self::getRouteLocale() !== null) {
            return '{locale?}';
        } else {
            return null;
        }
    }

    public static function validateLocale($locale)
    {
        return in_array($locale, config("locales.site_locales"));
    }

    public static function getLocalizedUrl($locale = null, $route = null)
    {
        $return_url = null;
        $curr_locale = request()->route('locale');

        if ($curr_locale === null) {
            if ($locale !== null) {
                $locale_path = request()->is('/') ? '/' . $locale : '/' . $locale . '/';
                if (! request()->is('/')) {
                    $return_url = request()->root() . $locale_path . request()->path();
                } else {
                    $return_url = request()->root() . $locale_path;
                }
                return $return_url;
            }
        }

//        dd(Route::currentRouteName());
//        dd(request(->query());
//        dd(\request()->query());
//        dd(request()->route('token'));

        URL::defaults(['locale' => $locale]);
        if ($route === null) {
            // TODO: Temporary workaround, remove later
            if (request()->route('token')) {
                $return_url = route(Route::currentRouteName(), request()->route('token'));
            } else {
                $return_url = route(Route::currentRouteName());
            }
        } else {
            $return_url = route($route);
        }


//        if ($reset_url) {
//        }
        // Reset locale to match current route
        URL::defaults(['locale' => $curr_locale]);

        return $return_url;
    }

    public static function getCurrLocale()
    {
        return App::getLocale();
    }

    public static function getClientLang($accept_lang_header)
    {
        $pref_langs = LocalizationHelper::clientPreferedLanguages($accept_lang_header);

        $prefered = $pref_langs->keys()->filter(function ($value) {
            return 2 == strlen($value);
        })->first(function ($value) {
            return $value === config("locales.default_locale") || self::validateLocale($value);
        });

        // Get fallback if no results
        if (null === $prefered) {
            $prefered = config("locales.default_locale");
        }
        return $prefered;
    }

    /**
     * It will return a list of prefered languages of the browser in order of preference.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function clientPreferedLanguages($prefered_languages)
    {
        // regex inspired from @GabrielAnderson on http://stackoverflow.com/questions/6038236/http-accept-language
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})*)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $prefered_languages,
            $lang_parse);
        $langs = $lang_parse[1];
        // Make sure the country chars (when available) are uppercase.
        $langs = collect($langs)->map(function ($lang) {
            if (5 == strlen($lang)) {
                $lang = explode('-', $lang);
                $lang = implode('-', [$lang[0], strtoupper($lang[1])]);
            }
            return $lang;
        })->toArray();
        $ranks = $lang_parse[4];
        // (create an associative array 'language' => 'preference')
        $lang2pref = [];
        for ($i = 0; $i < count($langs); $i++) {
            $lang2pref[$langs[$i]] = (float) (! empty($ranks[$i]) ? $ranks[$i] : 1);
        }
        // (comparison function for uksort)
        $cmpLangs = function ($a, $b) use ($lang2pref) {
            if ($lang2pref[$a] > $lang2pref[$b]) {
                return -1;
            } elseif ($lang2pref[$a] < $lang2pref[$b]) {
                return 1;
            } elseif (strlen($a) > strlen($b)) {
                return -1;
            } elseif (strlen($a) < strlen($b)) {
                return 1;
            } else {
                return 0;
            }
        };
        // sort the languages by prefered language and by the most specific region
        uksort($lang2pref, $cmpLangs);
        return collect($lang2pref);
    }
}