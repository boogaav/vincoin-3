@extends("layouts.main")
@inject('reward', 'App\Services\RewardSystem\RewardHelper')
@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")

@section("content")
  <header class="main-header js-main-header--default">
    <div class="main-header__container">
      <!-- Hamburger icon -->
      <div class="main-header__container__nav-trigger js-nav-trigger"><span>@lang("navigation.main.Menu")</span></div>
      <div class="nav-desktop">
        <div class="logo-wrapper">
          <a href="{{ route('site.home') }}" class="logo">
            <span class="logo__brand-img"></span>
            <span class="logo__brand-name">Vincoin Cash</span>
          </a>
        </div>
        <div class="nav-sections">
          <div class="nav-secondary">
            <nav role="navigation" class="nav-wrapper">
              <ul class="nav">
                <li class="nav__item"><a class="nav__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                @auth
                  <li class="nav__item">
                    <a class="nav__link" href="javascript:void(0);">
                      <div class="flex items-center">
                        {{ auth()->user()->email  }} <span class="nav__link__arrow"></span>
                      </div>
                    </a>
                    <div class="nav__dropdown">
                      <div class="nav__dropdown__cover"></div>
                      <ul class="nav__dropdown__list">
                        <li class="nav__dropdown__item"><a href="{{ route("site.showAccount") }}" class="nav__link">@lang('navigation.main.Account')</a></li>
                        <li class="nav__dropdown__item">
                          <a href="{{ route('auth.logout') }}" class="nav__link" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
                          <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                          </form>
                        </li>
                      </ul>
                    </div>
                  </li>
                @endauth
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="language-wrapper">
        <div class="language-selector nav__item">
          <a class="nav__link" href="javascript:void(0);">
            <div class="flex items-center">
              {{ $localization_helper->getCurrLocale() }} <span class="nav__link__arrow"></span>
            </div>
          </a>
          <div class="nav__dropdown">
            <div class="nav__dropdown__cover"></div>
            <ul class="nav__dropdown__list js-select-language">
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl() }}" class="nav__link">EN</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('ru') }}" class="nav__link">RU</a></li>
              <li class="nav__dropdown__item"><a href="{{ $localization_helper->getLocalizedUrl('vn') }}" class="nav__link">VN</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="download-btn">
        <button class="btn" type="button"><a class="uppercase" href="{{ route("site.home") }}#section-wallet">@lang('navigation.main.Testnet is live')</a></button>
      </div>
    </div>
  </header>

  <div class="nav-mobile">
    <div class="nav-mobile__control nav-mobile__control--back btn-mobile--icon-back js-nav-trigger"><span>@lang("navigation.main.Back")</span></div>
    @auth
      <a href="{{ route('site.showAccount') }}" class="nav-mobile__control nav-mobile__control--user btn-mobile--icon-user"><span>@lang('navigation.main.Account')</span></a>
    @endauth
    <div class="logo-wrapper flex-no-shrink">
      <div class="inline-block">
        <a href="{{ route('site.home') }}" class="logo logo--white logo--col">
          <span class="logo__brand-img"></span>
          <span class="logo__brand-name">Vincoin Cash</span>
        </a>
      </div>
    </div>
    <div class="flex-shrink overflow-scroll">
      <!-- Navigation -->
      <nav id="ml-menu" role="navigation" class="nav-wrapper ml-menu">
        <div class="menu__wrap">
          <ul data-menu="main" class="nav nav--col menu__level">
            <li class="nav__item menu__item"><a class="nav__link menu__link" href="javascript:void(0);" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
            @auth
              <li class="nav__item menu__item">
                <a class="nav__link menu__link" data-submenu="submenu-2" href="javascript:void(0);">{{ auth()->user()->email  }}</a>
              </li>
            @endauth
          </ul>
          <ul data-menu="submenu-2" class="nav nav--col menu__level">
            <li class="nav__item menu__item"><a class="nav__link menu__link" href="{{ route("site.showAccount") }}">@lang('navigation.main.Account')</a></li>
            <li class="nav__item menu__item">
              <a class="nav__link menu__link" href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">@lang('navigation.main.Log out')</a>
              <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
            <li class="nav__item menu__item"><a class="nav__link menu__link submenu-back uppercase" href="javascript:void(0);">
                <span class="back-arrow"></span>
                @lang('navigation.main.Back')
              </a></li>
          </ul>
        </div>
      </nav>
    </div>
    <div class="mt-2 mb-2 mx-auto"><button data-fancybox-close class="btn btn--transparent normal-case min-w-160 js-nav-trigger" type="button">@lang("navigation.main.Close Menu")</button></div>
  </div>

  <div class="section-account">
    <div class="section-account__container container">
      <div class="account-container">
        <div class="flex justify-between md:flex-col-reverse">
          <h2 class="heading heading--md heading--bordered mb-6">@lang("user.User information")</h2>
          <div class="account-airDrop-notify mb-6 hidden">
            <a href="https://t.me/Vincoinbot" class="account-airDrop-notify__wrap" target="_blank">
              @lang("Join the hottest Vincoin Cash Airdrop")
            </a>
          </div>
        </div>
        <div class="panel account-details">
          <div class="panel-section panel-section--primary">
            <div class="panel-section__title flex justify-between align-middle">
              <h3 class="heading heading--sm">@lang("user.Profile")</h3>
              @if($user->hasBonusTokens())
                <div class="tokens-earned{{ !$user->hasBonusTokens() ? ' hidden': '' }}"><span class="coin-icon"></span>x <span class="amount">{{ $user->profile->bonus_tokens }}</span></div>
              @endif
            </div>
            <div class="panel-section__body">
              <ul class="account-details__list">
                <li class="account-details__item">
                  <div class="account-details__item-info account-edit-field" style="{{ empty($user->profile['wallet_number']) ? 'display:none;' : '' }}">
                    <div class="account-details__label"><span>@lang("user.Wallet number")</span></div>
                    <div class="account-details__text text-overflow-fix">
                      {{ $user->profile['wallet_number'] }}
                    </div>
                    <div class="account-details__action-btns">
                      <a href="javascript:void(0);" class="link js-account-edit-btn">@lang("user.Edit field")</a>
                    </div>
                  </div>
                  <div class="account-details__item-input" style="{{ !empty($user->profile['wallet_number']) ? 'display:none;' : '' }}">
                    <div class="account-details__label"><span>@lang("user.Add your wallet number:")</span></div>
                    <div class="account-details__text js-form-container">
                      <form action="{{ route('site.updateAccount') }}" class="vin-form vin-form--transparent vin-form--inline profile-form" method="POST">
                        <div class="form__holder js-form-content">
                          <div class="form-group"><input type="text" id="wallet_number" name="wallet_number" class="form-control form-control--md" value="{{ old('wallet_number', $user->profile['wallet_number']) }}" placeholder="Wallet number" required></div>
                          <button class="btn btn--red btn--small submit-btn ladda-button update-acc-btn" type="submit" data-style="expand-right"><span class="ladda-label">@lang("user.Add")</span></button>
                        </div>
                        <div class="success-msg-wrapper js-form-success-msg">@lang("user.Account updated!")</div>
                      </form>
                    </div>
                  </div>
                </li>
                <li class="account-details__item">
                  <div class="account-details__label"><span>Email</span></div>
                  <div class="account-details__text text-overflow-fix">{{ $user['email'] }}</div>
                </li>
                <li class="account-details__item">
                  <div class="account-details__label"><span>@lang("user.Referrer link")</span></div>
                  <div class="account-details__text text-overflow-fix"><a class="link" href="{{ $ref_link['url'] }}" target="_blank">{{ $ref_link['url'] }}</a></div>
                </li>
              </ul>
            </div>
          </div>
          <div class="panel-section panel-section--secondary">
            <div class="panel-section__title"><h3 class="heading heading--sm">@lang("user.Social accounts")</h3></div>
            <div class="panel-section__body">
              <ul class="account-details__list">
                <li class="account-details__item">
                  <div class="account-details__label"><span>@lang("user.Join our Telegram group:")</span></div>
                  <div class="account-details__text">
                    <button class="btn btn--red btn--small"><a href="https://t.me/vincoincash" target="_blank">@lang("user.Join")</a></button>
                  </div>
                </li>
                <li class="account-details__item">
                  <div class="account-details__item-info account-edit-field" style="{{ empty($user->profile['telegram_nickname']) ? 'display:none;' : '' }}">
                    <div class="account-details__label"><span>@lang("user.Your Telegram username")</span></div>
                    <div class="account-details__text text-overflow-fix">
                      {{ $user->profile['telegram_nickname'] }}
                    </div>
                    <div class="account-details__action-btns">
                      <a href="javascript:void(0);" class="link js-account-edit-btn">@lang("user.Edit field")</a>
                    </div>
                  </div>
                  <div class="account-details__item-input" style="{{ !empty($user->profile['telegram_nickname']) ? 'display:none;' : '' }}">
                    <div class="account-details__label"><span>@lang("user.Add your Telegram username:")</span></div>
                    <div class="account-details__text js-form-container">
                      <form action="{{ route('site.updateAccount') }}" class="vin-form vin-form--transparent vin-form--inline profile-form" method="POST" data-tokens="{{ $reward::BONUS_AMOUNT_B }}">
                        <div class="form__holder js-form-content">
                          <div class="form-group"><input type="text" id="telegram_nickname" name="telegram_nickname" class="form-control" value="{{ old('telegram_nickname', $user->profile['telegram_nickname']) }}" placeholder="@lang("user.Telegram nickname")" required></div>
                          <button class="btn btn--red btn--small submit-btn ladda-button update-acc-btn" type="submit" data-style="expand-right"><span class="ladda-label">@lang("user.Add")</span></button>
                        </div>
                        <div class="success-msg-wrapper js-form-success-msg">@lang("user.Account updated!")</div>
                      </form>
                      {{--<div class="tokens-potential">( @lang("user.Earn up to") <span class="amount">{{ $reward::BONUS_AMOUNT_B }}</span> <span class="coin-icon"></span>)</div>--}}
                    </div>
                  </div>
                </li>
                <li class="account-details__item">
                  <div class="account-details__label"><span>@lang("user.Subscribe to Facebook:")</span></div>
                  <div class="account-details__text">
                    <button class="btn btn--red btn--small join-group update-acc-btn" data-group="facebook" data-action="{{ route('site.updateAccount') }}" data-tokens="{{ $reward::BONUS_AMOUNT_A }}"><a href="https://www.facebook.com/vincoincash/" target="_blank">@lang("user.Join")</a></button>
                    {{--@if(!$user->profile['is_facebook_joined'])--}}
                      {{--<div class="tokens-potential">( @lang("user.Earn up to") <span class="amount">{{ $reward::BONUS_AMOUNT_A }}</span> <span class="coin-icon"></span>)</div>--}}
                    {{--@endif--}}
                  </div>
                </li>
                <li class="account-details__item">
                  <div class="account-details__label"><span>@lang("user.Subscribe to Twitter:")</span></div>
                  <div class="account-details__text">
                    <button class="btn btn--red btn--small join-group update-acc-btn" data-group="twitter" data-action="{{ route('site.updateAccount') }}" data-tokens="{{ $reward::BONUS_AMOUNT_A }}"><a href="https://twitter.com/Vincoin_Cash" target="_blank">@lang("user.Join")</a></button>
                    {{--@if(!$user->profile['is_twitter_joined'])--}}
                      {{--<div class="tokens-potential">( @lang("user.Earn up to") <span class="amount">{{ $reward::BONUS_AMOUNT_A }}</span> <span class="coin-icon"></span>)</div>--}}
                    {{--@endif--}}
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection